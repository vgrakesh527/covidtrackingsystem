package com.rak.springboot.covidtrackingsystem.rest;

public class UserNotFoundExceptions extends RuntimeException {

	public UserNotFoundExceptions(String message, Throwable cause) {
		super(message, cause);
	}

	public UserNotFoundExceptions(String message) {
		super(message);
	}

	public UserNotFoundExceptions(Throwable cause) {
		super(cause);
	}

	
}
