package com.rak.springboot.covidtrackingsystem.entity;

public class Patients {

	private String country;
	private long confirmed;
	private long recovered;
	private long deaths;
	private long active;
	
	
	
	public Patients(String country, long confirmed, long recovered, long deaths, long active) {
		this.country = country;
		this.confirmed = confirmed;
		this.recovered = recovered;
		this.deaths = deaths;
		this.active = active;
	}
	
	
	public Patients() {

	}

	@Override
	public String toString() {
		return "Patients [country=" + country + ", confirmed=" + confirmed + ", recovered=" + recovered + ", deaths="
				+ deaths + ", active=" + active + "]";
	}


	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public long getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(long confirmed) {
		this.confirmed = confirmed;
	}
	public long getRecovered() {
		return recovered;
	}
	public void setRecovered(long recovered) {
		this.recovered = recovered;
	}
	public long getDeaths() {
		return deaths;
	}
	public void setDeaths(long deaths) {
		this.deaths = deaths;
	}
	public long getActive() {
		return active;
	}
	public void setActive(long active) {
		this.active = active;
	}
	
	
}
