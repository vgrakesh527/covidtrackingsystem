package com.rak.springboot.covidtrackingsystem.rest;

public class UserExceptions extends RuntimeException {

	public UserExceptions(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public UserExceptions(String arg0) {
		super(arg0);
	}

	public UserExceptions(Throwable arg0) {
		super(arg0);
	}

}
