package com.rak.springboot.covidtrackingsystem.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rak.springboot.covidtrackingsystem.entity.Patients;
import com.rak.springboot.covidtrackingsystem.entity.Response;
import com.rak.springboot.covidtrackingsystem.entity.User;
import com.rak.springboot.covidtrackingsystem.service.CovidTrackingService;

@RestController
public class CovidTrackingRestController {

	private List<Patients> storePatients = new ArrayList<Patients>();
	private List<User> storeUser = new ArrayList<User>();
	private Response response = new Response();
	private CovidTrackingService covidTrackingService;
	
	@Autowired
	public CovidTrackingRestController(CovidTrackingService covidTrackingService) {
		this.covidTrackingService = covidTrackingService;
	}

	@PostConstruct
	public List<Patients> showPatients()
	{
		storePatients.add(new Patients("Brazil", 10869739, 5754006, 521298, 5564370));
		storePatients.add(new Patients("USA", 32424, 123444, 44, 12666));
		storePatients.add(new Patients("Peru", 77343, 223, 556, 676889));

		return storePatients;
	}
	
	@PostConstruct
	public List<User> showUser()
	{
		storeUser.add(new User("user1", "abcabc123"));
		return storeUser;
	}
	
	@PostMapping("/register")
	public Response addUser(@RequestBody User user)
	{
		storeUser = covidTrackingService.saveUser(storeUser, user);
		response.setData("user "+user.getUsername()+" registered successfully");
		return response;
	}
	
	@DeleteMapping("/register")
	public Response deleteUser(@RequestBody Map<String, String>userDetails)
	{
		String username = userDetails.get("userName");
		System.out.println(username);
		User theUser = new User(username, null);
		storeUser = covidTrackingService.deleteUser(storeUser, theUser);
		response.setData("user deleted successfully");
		return response;
	}
	
	@GetMapping("/corona/username={username}&password={password}")
	public List<Patients> returnAllPatients(@PathVariable("username")String userName,@PathVariable("password")String password)
	{
		User theUser = new User(userName, password);
		System.out.println(theUser.getUsername()+"  "+ theUser.getPassword());
		boolean userValidation = covidTrackingService.userValidation(storeUser, theUser);
		if(!userValidation)
		{
			throw new UserExceptions("Invalid Username or Password");
		}
		return storePatients;
	}
	@GetMapping("/corona/username={username}&password={password}&country={country}")
	public List<Patients> returnAllPatientsByCategory(@PathVariable Map<String, String> pathVarsMap)
	{
		String country = pathVarsMap.get("country");
		String userName = pathVarsMap.get("username");
		String password = pathVarsMap.get("password");
		System.out.println("Country "+ country);
		User theUser = new User(userName, password);
		System.out.println(theUser.getUsername()+"  "+ theUser.getPassword());
		boolean userValidation = covidTrackingService.userValidation(storeUser, theUser);
		if(!userValidation)
		{
			throw new UserExceptions("Invalid Username or Password");
		}
		List<Patients>patientsList = covidTrackingService.returnAllPatientsByCategory(storePatients, country);
		if(patientsList == null || patientsList.isEmpty())
		{
			throw new UserExceptions("Country Details not found");
		}
		return patientsList;
	}
	
	@GetMapping("/users")
	public List<User> allUsers()
	{
		return storeUser;
	}
}
