package com.rak.springboot.covidtrackingsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.rak.springboot.covidtrackingsystem.entity.Patients;
import com.rak.springboot.covidtrackingsystem.entity.User;
import com.rak.springboot.covidtrackingsystem.rest.UserExceptions;
import com.rak.springboot.covidtrackingsystem.rest.UserNotFoundExceptions;

@Service
public class CovidTrackingServiceImpl implements CovidTrackingService {

	@Override
	public List<User> saveUser(List<User> storeUser, User theUser) {
		if(0 <= userExist(storeUser,theUser))
		{
			throw new UserExceptions("User is already registered");
		}
		if(!passwordLength(theUser.getPassword()))
		{
			throw new UserExceptions("Minimum length of password is 8 and minimum 2 digits required");
		}
		storeUser.add(theUser);
		return storeUser;
	}

	@Override
	public List<User> deleteUser(List<User> storeUser, User theUser) {
		int index = userExist(storeUser,theUser);
		if(0 > index)
		{
			throw new UserNotFoundExceptions("user does not Exist");
		}
		storeUser.remove(index);
		return storeUser;
	}

	@Override
	public List<Patients> returnAllPatientsByCategory(List<Patients> storePatients, String country) {
		
		List<Patients>patientsList = new ArrayList<Patients>();
		for(Patients thePatients:storePatients)
		{
			if(country.equals(thePatients.getCountry()))
			{
				patientsList.add(thePatients);
			}
		}
		
		return patientsList;
	}

	private int userExist(List<User> storeUser,User theUser)
	{
		for(User tempUser : storeUser)
		{
			if(tempUser.getUsername().equals(theUser.getUsername()))
			{
				return storeUser.indexOf(tempUser);
			}
		}
		return -1;
	}
	
	@Override
	public boolean userValidation(List<User> storeUser,User theUser)
	{
		for(User tempUser : storeUser)
		{
			if(tempUser.getUsername().equals(theUser.getUsername())
					&&tempUser.getPassword().equals(theUser.getPassword()))
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean passwordLength(String password) {
		boolean correct = true;
		int digit = 0;
		if (password.length() < 8) {
			return false;
		}
		char element;
		for (int index = 0; index < password.length(); index++) {

			element = password.charAt(index);

			if (Character.isDigit(element)) {
				digit++;
			}

		}
		if (digit < 2) {
			return false;
		}
		if (!password.matches("[a-zA-Z0-9]+")) {
			return false;
		}
		return correct;

	}
}
