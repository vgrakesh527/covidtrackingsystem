package com.rak.springboot.covidtrackingsystem.service;

import java.util.List;

import com.rak.springboot.covidtrackingsystem.entity.Patients;
import com.rak.springboot.covidtrackingsystem.entity.User;

public interface CovidTrackingService {

	public List<User> saveUser(List<User>storeUser,User theUser);
	
	public List<User> deleteUser(List<User>storeUser,User theUser);
	
	
	public List<Patients> returnAllPatientsByCategory(List<Patients>storePatients,String country);

	boolean userValidation(List<User> storeUser, User theUser);
}
