package com.rak.springboot.covidtrackingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidtrackingsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidtrackingsystemApplication.class, args);
	}

}
