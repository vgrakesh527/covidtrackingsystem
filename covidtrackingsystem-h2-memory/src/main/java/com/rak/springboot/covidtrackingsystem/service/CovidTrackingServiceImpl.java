package com.rak.springboot.covidtrackingsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rak.springboot.covidtrackingsystem.dao.CovidTrackingDao;
import com.rak.springboot.covidtrackingsystem.dao.CovidTrackingPatientsDao;
import com.rak.springboot.covidtrackingsystem.entity.Patients;
import com.rak.springboot.covidtrackingsystem.entity.User;
import com.rak.springboot.covidtrackingsystem.rest.PatiensNotFoundExceptions;
import com.rak.springboot.covidtrackingsystem.rest.UserExceptions;
import com.rak.springboot.covidtrackingsystem.rest.UserNotFoundExceptions;

@Service
public class CovidTrackingServiceImpl implements CovidTrackingService {

	@Autowired
	private CovidTrackingDao covidTrackingDao;
	
	@Autowired
	private CovidTrackingPatientsDao covidTrackingPatientsDao;
	
	@Override
	public void storePatients() {
		covidTrackingPatientsDao.save(new Patients("Brazil", 10869739, 5754006, 521298, 5564370));
		covidTrackingPatientsDao.save(new Patients("USA", 32424, 123444, 44, 12666));
		covidTrackingPatientsDao.save(new Patients("Peru", 77343, 223, 556, 676889));
	}
	
	private boolean passwordLength(String password) {
		boolean correct = true;
		int digit = 0;
		if (password.length() < 8) {
			return false;
		}
		char element;
		for (int index = 0; index < password.length(); index++) {

			element = password.charAt(index);

			if (Character.isDigit(element)) {
				digit++;
			}

		}
		if (digit < 2) {
			return false;
		}
		if (!password.matches("[a-zA-Z0-9]+")) {
			return false;
		}
		return correct;

	}

	@Override
	public List<User> returnUser() {
		List<User> usersList = new ArrayList<User>();
		covidTrackingDao.findAll().forEach(user ->{
			usersList.add(user);
		});
		return usersList;
	}
	boolean returnUser = false;
	@Override
	public void saveUser(User user) {
		covidTrackingDao.findAll().forEach(tempUser ->
		{
			 if(tempUser.getUsername().equals(user.getUsername()))
			 {
				 returnUser = true;
			 }
		}); 
		if(returnUser)
		{
			returnUser = false;
			throw new UserExceptions("User is already registered");
		}
		if(!passwordLength(user.getPassword()))
		{
			throw new UserExceptions("Password Length must be 8 and minimum 2 digits required");
		}
		covidTrackingDao.save(user);
	}

	private int userId = 0;
	@Override
	public int deleteUser(String userName) {
		userId = 0;
		covidTrackingDao.findAll().forEach(tempUser ->
		{
			if(userName.equals(tempUser.getUsername()))
			{
				userId = tempUser.getUserId();
			}
		});
		if(userId <= 0)
		{
			throw new UserNotFoundExceptions("user does not Exist");
		}
		covidTrackingDao.deleteById(userId);
		return userId;
	}
	boolean userExistFlag = false;
	@Override
	public List<Patients> returnPatients(User user) {
		userExistFlag = false;
		covidTrackingDao.findAll().forEach(tempUser ->{
			if(tempUser.getUsername().equals(user.getUsername()) && tempUser.getPassword().equals(user.getPassword()))
			{
				userExistFlag = true;
			}
		});
		
		if(!userExistFlag)
		{
			throw new UserNotFoundExceptions("Invalid/Missing User name or Password");
		}
		List<Patients> patientsList = new ArrayList<Patients>();
		covidTrackingPatientsDao.findAll().forEach(patients ->{
			patientsList.add(patients);
		});
		if(patientsList == null || patientsList.isEmpty())
		{
			throw new PatiensNotFoundExceptions("Patients does not Exist");
		}
		return patientsList;
	}

	@Override
	public List<Patients> returnPatients(User user, String country) {
		userExistFlag = false;
		covidTrackingDao.findAll().forEach(tempUser ->{
			if(tempUser.getUsername().equals(user.getUsername()) && tempUser.getPassword().equals(user.getPassword()))
			{
				userExistFlag = true;
			}
		});
		
		if(!userExistFlag)
		{
			throw new UserNotFoundExceptions("Invalid/Missing User name or Password");
		}
		List<Patients> patientsList = new ArrayList<Patients>();
		covidTrackingPatientsDao.findAll().forEach(patients ->{
			if(country.equals(patients.getCountry()))
			{
				patientsList.add(patients);
			}
		});
		if(patientsList == null || patientsList.isEmpty())
		{
			throw new PatiensNotFoundExceptions("Patients Details does not Exist");
		}
		return patientsList;
	}

	
}
