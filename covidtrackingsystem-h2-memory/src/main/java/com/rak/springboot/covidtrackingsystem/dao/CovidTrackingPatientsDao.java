package com.rak.springboot.covidtrackingsystem.dao;

import org.springframework.data.repository.CrudRepository;

import com.rak.springboot.covidtrackingsystem.entity.Patients;

public interface CovidTrackingPatientsDao extends CrudRepository<Patients, Integer>{

}
