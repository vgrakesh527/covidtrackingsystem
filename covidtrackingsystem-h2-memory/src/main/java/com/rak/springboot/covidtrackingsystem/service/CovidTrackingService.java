package com.rak.springboot.covidtrackingsystem.service;

import java.util.List;

import com.rak.springboot.covidtrackingsystem.entity.Patients;
import com.rak.springboot.covidtrackingsystem.entity.User;

public interface CovidTrackingService {

	public List<User> returnUser();
	
	public void saveUser(User user);
	
	public int deleteUser(String userName);
	
	public void storePatients();
	
	public List<Patients> returnPatients(User user);
	
	public List<Patients> returnPatients(User user,String country);
}
