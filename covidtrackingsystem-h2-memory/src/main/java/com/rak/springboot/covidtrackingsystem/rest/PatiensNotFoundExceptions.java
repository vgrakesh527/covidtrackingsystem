package com.rak.springboot.covidtrackingsystem.rest;

public class PatiensNotFoundExceptions extends RuntimeException {

	public PatiensNotFoundExceptions(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public PatiensNotFoundExceptions(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public PatiensNotFoundExceptions(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
