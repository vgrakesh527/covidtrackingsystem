package com.rak.springboot.covidtrackingsystem.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rak.springboot.covidtrackingsystem.entity.Patients;
import com.rak.springboot.covidtrackingsystem.entity.Response;
import com.rak.springboot.covidtrackingsystem.entity.User;
import com.rak.springboot.covidtrackingsystem.service.CovidTrackingService;

@RestController
public class CovidTrackingRestController {

	private List<Patients> storePatients = new ArrayList<Patients>();
	private List<User> storeUser = new ArrayList<User>();
	private Response response = new Response();
	private CovidTrackingService covidTrackingService;
	
	@Autowired
	public CovidTrackingRestController(CovidTrackingService covidTrackingService) {
		this.covidTrackingService = covidTrackingService;
	}
	
	@PostConstruct
	public void storePatiens()
	{
		covidTrackingService.storePatients();
	}
		
	@PostMapping("/register")
	public Response addUser(@RequestBody User user)
	{
		covidTrackingService.saveUser(user);
		if(user.getUserId() > 0)
		{
			response.setData("user "+user.getUsername()+" registered successfully");
		}
		return response;
	}
	
	@DeleteMapping("/register")
	public Response deleteUser(@RequestBody Map<String, String>userDetails)
	{
		String username = userDetails.get("userName");
		System.out.println(username);
		int status = covidTrackingService.deleteUser(username);
		if(status > 0)
		{
			response.setData("user deleted successfully");
		}
		return response;
	}
	
	@GetMapping("/corona/username={username}&password={password}")
	public List<Patients> returnAllPatients(@PathVariable("username")String userName,@PathVariable("password")String password)
	{
		User theUser = new User(userName, password);
		storePatients = covidTrackingService.returnPatients(theUser);
		return storePatients;
	}
	
	@GetMapping("/corona/username={username}&password={password}&country={country}")
	public List<Patients> returnAllPatientsByCategory(@PathVariable Map<String, String> pathVarsMap)
	{
		String country = pathVarsMap.get("country");
		String userName = pathVarsMap.get("username");
		String password = pathVarsMap.get("password");
		System.out.println("Country "+ country);
		User theUser = new User(userName, password);
		storePatients = covidTrackingService.returnPatients(theUser,country);
		return storePatients;
	}
	
	@GetMapping("/users")
	public List<User> allUsers()
	{
		storeUser = covidTrackingService.returnUser();
		return storeUser;
	}
}
