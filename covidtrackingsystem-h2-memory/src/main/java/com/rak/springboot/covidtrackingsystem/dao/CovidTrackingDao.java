package com.rak.springboot.covidtrackingsystem.dao;

import org.springframework.data.repository.CrudRepository;

import com.rak.springboot.covidtrackingsystem.entity.User;

public interface CovidTrackingDao extends CrudRepository<User, Integer> {

}
